# jfxs / Demo Gitlab-ci Robot Framework

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/projects/demo-gitlab-ci-robot-framework//badges/master/pipeline.svg)](https://gitlab.com/op_so/projects/demo-gitlab-ci-robot-framework/pipelines)
[![Robot Framework Tests](https://op_so.gitlab.io/projects/demo-gitlab-ci-robot-framework/all_tests.svg)](https://op_so.gitlab.io/projects/demo-gitlab-ci-robot-framework/report.html)

An example of project to run [Robot Framework](https://robotframework.org/) tests in a [Gitlab-ci](https://docs.gitlab.com/ce/ci/) pipeline.

Last test report is published with Gitlab pages and is available [here](https://op_so.gitlab.io/projects/demo-gitlab-ci-robot-framework/report.html).

## Run tests locally (debug)

In order to run tests locally you need docker and make.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

Example to run Robot Framework tests in your current directory:

```shell
# To show available commands:
make
# Example: to run Selenium tests with Chrome browser:
make tests-local browser=gc
```

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
